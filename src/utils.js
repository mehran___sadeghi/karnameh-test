// lazy clone of object and change the variables
export const updateObject = (oldState, newState) => {
    return { ...oldState, ...newState };
};
