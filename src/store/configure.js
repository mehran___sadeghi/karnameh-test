import {
    combineReducers,
    legacy_createStore as createStore,
    compose,
    applyMiddleware,
} from "redux";
import thunk from "redux-thunk";

//#region import reducers
import questionReducer from "./reducers/questions";
//#endregion

const rootReducer = combineReducers({
    questions: questionReducer,
});

// compose redux devtools enhancers
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
);

export default store;
