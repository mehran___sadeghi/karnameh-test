import { updateObject } from "../../utils";
import {
    ADD_COMMENT,
    SET_ERROR,
    SET_LOADING,
    SET_QUESTIONS,
    SET_QUESTION_DETAILS,
    UPDATE_REACTION,
} from "./../actions/questions";

const initialState = {
    questions: [],
    question_details: null,
    loading: false,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_LOADING:
            return updateObject(state, { loading: action.payload });
        case SET_ERROR:
            return updateObject(state, { error: action.payload });
        case SET_QUESTIONS:
            return updateObject(state, { questions: action.payload });
        case SET_QUESTION_DETAILS:
            return updateObject(state, { question_details: action.payload });
        case UPDATE_REACTION:
            const newState = {
                ...state,
                question_details: {
                    ...state.question_details,
                    replies: [...state.question_details.replies],
                },
            };
            newState.question_details.replies[action.payload.index] =
                action.payload.data;
            return newState;
        case ADD_COMMENT:
            return updateObject(state, { question_details: action.payload });
        default:
            return state;
    }
};

export default reducer;
