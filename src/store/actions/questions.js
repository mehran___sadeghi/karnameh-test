import API from "../../api";

//#region Action types
export const SET_QUESTIONS = "SET_QUESTIONS";
export const SET_QUESTION_DETAILS = "SET_QUESTION_DETAILS";
export const SET_LOADING = "SET_LOADING";
export const SET_ERROR = "SET_ERROR";
export const UPDATE_REACTION = "UPDATE_REACTION";
export const ADD_COMMENT = "ADD_COMMENT";
export const ADD_QUESTION = "ADD_QUESTION";
//#endregion

/**
 * gets the list of all questions.
 * then stores the result in redux store
 */
export const getQuestions = () => {
    return (dispatch) => {
        dispatch({ type: SET_LOADING, payload: true });
        dispatch({ type: SET_ERROR, payload: null });
        API.get.questions().then((res) => {
            if (res.status === 200) {
                dispatch({ type: SET_QUESTIONS, payload: res.data });
                dispatch({ type: SET_LOADING, payload: false });
            }
        });
    };
};

/**
 * get the detils of question and store it in redux store.
 * @param {Number} id id of desired question
 */
export const getQuestionDetails = (id) => {
    return (dispatch) => {
        dispatch({ type: SET_LOADING, payload: true });
        dispatch({ type: SET_ERROR, payload: null });
        API.get.questionDetails(id).then((res) => {
            if (res.status === 200) {
                dispatch({ type: SET_QUESTION_DETAILS, payload: res.data });
                dispatch({ type: SET_LOADING, payload: false });
            }
        });
    };
};

/**
 * handles the positive and nagative reaction.
 * handles it in both api and redux store
 * @param {Number} id id of desired question
 * @param {Object} newData desired data for put in database and redux store
 * @param {Nuber} index index of desired question
 */
export const ReplyReaction = (id, newData, index) => {
    return (dispatch) => {
        API.put.reply(id, newData).then((res) => {
            if (res.status === 200) {
                dispatch({
                    type: UPDATE_REACTION,
                    payload: { data: res.data, index: index },
                });
            }
        });
    };
};

/**
 * @param {Number} id id of desired question
 * @param {String} text comment
 */
export const AddComment = (id, text) => {
    return (dispatch, getState) => {
        const { questions } = getState();
        const currentDate = new Date();
        const newData = {
            ...questions.question_details,
            replies: [...questions.question_details.replies],
        };
        /**
         * It is assumed that the login system is disabled.
         * Therefore, I put the following data (like avatar, name, ...) statically
         */
        newData.replies.push({
            id: [...new Array(7)]
                .map((i) => parseInt(Math.random() * 50).toString(32))
                .join(""),
            name: "الناز شاکردوست",
            avatar: "https://as1.ftcdn.net/v2/jpg/03/67/46/48/1000_F_367464887_f0w1JrL8PddfuH3P2jSPlIGjKU2BI0rn.jpg",
            description: text,
            date: currentDate.toLocaleDateString("fa-IR"),
            time: currentDate
                .toLocaleTimeString("fa-IR")
                .match(/^(.*):..?$/)[1],
            like_number: 0,
            has_liked: false,
            dislike_number: 0,
            has_disliked: false,
        });
        API.put.reply(id, newData).then((res) => {
            if (res.status === 200) {
                dispatch({ type: ADD_COMMENT, payload: newData });
            }
        });
    };
};

/**
 * @param {String} subject
 * @param {String} text
 */
export const AddNewQuestion = (subject, text) => {
    return (dispatch) => {
        const currentDate = new Date();
        API.post
            .newQuestion({
                title: subject,
                description: text,
                avatar: "https://as1.ftcdn.net/v2/jpg/03/67/46/48/1000_F_367464887_f0w1JrL8PddfuH3P2jSPlIGjKU2BI0rn.jpg",
                replies: [],
                date: currentDate.toLocaleDateString("fa-IR"),
                time: currentDate
                    .toLocaleTimeString("fa-IR")
                    .match(/^(.*):..?$/)[1],
            })
            .then((res) => {
                if (res.status === 201) {
                    dispatch(getQuestions());
                }
            });
    };
};
