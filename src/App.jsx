import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import QuestionDetails from "./views/pages/question-details";

// import pages
import Questions from "./views/pages/questions";

const App = () => {
    return (
        <>
            <Routes>
                <Route exact path="/" element={<Navigate to="/questions" />} />
                <Route exact path="/questions" element={<Questions />} />
                <Route
                    exact
                    path="/question-details/:id"
                    element={<QuestionDetails />}
                />
            </Routes>
        </>
    );
};

export default App;
