import * as apiList from "./api-list";

/**
 * access apis like API.get.questions().then(...)
 */
const API = {
    get: {
        questions: apiList.getQuestions,
        questionDetails: apiList.getQuestionDetails,
    },
    post: {
        newQuestion: apiList.postNewQuestion,
    },
    put: {
        reply: apiList.putReply,
    },
};

export default API;
