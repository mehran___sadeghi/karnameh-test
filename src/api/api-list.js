import axios from "./axios-order";

/**
 * list of all requests goes here.
 * i listed only in one file because this project is small.
 * otherwise i put theme in multi files based on api category
 */

export const getQuestions = () => {
    return axios.get("/questions");
};

export const getQuestionDetails = (id) => {
    return axios.get(`/questions/${id}`);
};

export const putReply = (id, data) => {
    return axios.put(`/questions/${id}`, data);
};

export const postNewQuestion = (data) => {
    return axios.post(`/questions`, data);
};
