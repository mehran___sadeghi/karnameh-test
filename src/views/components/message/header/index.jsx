import React from "react";
import styles from "./header.module.css";
import PropTypes from "prop-types";

// icons
import { ReactComponent as CommentIcon } from "./../../../../assets/svg/comment.svg";
import { ReactComponent as HappyIcon } from "./../../../../assets/svg/happy.svg";
import { ReactComponent as SadIcon } from "./../../../../assets/svg/sad.svg";

const Header = (props) => {
    const {
        avatar,
        isReply,
        name,
        title,
        replies,
        has_disliked,
        dislike_number,
        has_liked,
        like_number,
        date,
        time,
    } = props;
    return (
        <div className={styles.Header}>
            <div className={styles.HeaderRight}>
                <div className={styles.ImageContainer}>
                    <img src={avatar} width={32} height={32} alt={"avatar"} />
                </div>
                <span>{isReply ? name : title}</span>
            </div>
            <div className={styles.HeaderLeft}>
                {!isReply && (
                    <div className={`${styles.InnerContainer} ${styles.Space}`}>
                        <CommentIcon className={styles.CommentIcon} />
                        <span className={styles.CommentsNumber}>
                            {replies.length}
                        </span>
                    </div>
                )}
                {isReply && (
                    <>
                        <div
                            className={`${styles.InnerContainer} ${styles.Space}`}
                        >
                            <SadIcon
                                className={`${styles.SadIcon} ${
                                    has_disliked ? styles.Active : ""
                                }`}
                            />
                            <span className={styles.CommentsNumber}>
                                {dislike_number}
                            </span>
                        </div>
                        <div
                            className={`${styles.InnerContainer} ${styles.Space}`}
                        >
                            <HappyIcon
                                className={`${styles.HappyIcon} ${
                                    has_liked ? styles.Active : ""
                                }`}
                            />
                            <span className={styles.CommentsNumber}>
                                {like_number}
                            </span>
                        </div>
                    </>
                )}
                <span
                    className={`${styles.InnerContainer} ${styles.HideInMobile}`}
                >
                    <span className={styles.Label}>تاریخ: </span>
                    <span className={styles.Value}>{date}</span>
                </span>
                <div className={`${styles.Separator} ${styles.HideInMobile}`} />
                <span
                    className={`${styles.InnerContainer} ${styles.HideInMobile}`}
                >
                    <span className={styles.Label}>ساعت: </span>
                    <span className={styles.Value}>{time}</span>
                </span>
            </div>
        </div>
    );
};

Header.propTypes = {
    avatar: PropTypes.string,
    isReply: PropTypes.bool,
    name: PropTypes.string,
    title: PropTypes.string,
    replies: PropTypes.array,
    has_disliked: PropTypes.bool,
    dislike_number: PropTypes.number,
    has_liked: PropTypes.bool,
    like_number: PropTypes.number,
    date: PropTypes.string,
    time: PropTypes.string,
};

export default React.memo(Header);
