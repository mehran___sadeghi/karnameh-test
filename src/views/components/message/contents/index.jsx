import React from "react";
import { Link } from "react-router-dom";
import Button from "../../ui/button";
import styles from "./content.module.css";
import PropTypes from "prop-types";

// icons
import { ReactComponent as HappyIcon } from "./../../../../assets/svg/happy.svg";
import { ReactComponent as SadIcon } from "./../../../../assets/svg/sad.svg";

const Contents = (props) => {
    const {
        description,
        image,
        id,
        noPicture,
        detailButton,
        isReply,
        negativeReaction,
        positiveReaction,
    } = props;
    return (
        <div className={styles.ContentContainer}>
            <p className={styles.Des}>{description}</p>
            {!noPicture && image && (
                <img
                    src={image}
                    alt={"question pic"}
                    className={styles.Picture}
                />
            )}
            {detailButton && (
                <Link
                    className={styles.ButtonLink}
                    to={`/question-details/${id}`}
                >
                    <Button
                        label={"مشاهده جزییات"}
                        btn_fill={false}
                        className={styles.ButtonDetails}
                        btn_style={{ width: "100px" }}
                    ></Button>
                </Link>
            )}
            {isReply && (
                <div className={styles.GoodNotGoodContainer}>
                    <Button
                        onClick={negativeReaction}
                        btn_fill={false}
                        className={styles.NotGoodButton}
                        btn_style={{
                            themeColor: "#E4E9EC",
                            themeColorHover: "var(--kar-color-hot)",
                            themeColorActive: "var(--kar-color-hot-hover)",
                            width: "130px",
                            color: "var(--kar-color-hot)",
                        }}
                    >
                        <span>پاسخ خوب نبود</span>
                        <SadIcon />
                    </Button>
                    <Button
                        onClick={positiveReaction}
                        btn_fill={false}
                        className={styles.GoodButton}
                        btn_style={{
                            themeColor: "#E4E9EC",
                            themeColorHover: "var(--kar-color-primary)",
                            themeColorActive: "var(--kar-color-primary-hover)",
                            width: "130px",
                            color: "var(--kar-color-primary)",
                        }}
                    >
                        <span>پاسخ خوب بود</span>
                        <HappyIcon />
                    </Button>
                </div>
            )}
        </div>
    );
};

Contents.propTypes = {
    description: PropTypes.string,
    image: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    noPicture: PropTypes.bool,
    detailButton: PropTypes.bool,
    isReply: PropTypes.bool,
    negativeReaction: PropTypes.func,
    positiveReaction: PropTypes.func,
};

export default Contents;
