import React from "react";
import styles from "./message.module.css";
import Contents from "./contents";
import Header from "./header";
import { ReplyReaction } from "../../../store/actions/questions";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const Message = (props) => {
    const {
        data,
        noPicture = false,
        isReply = false,
        detailButton = false,
        reactToComment,
        question_details,
    } = props;

    const positiveReaction = () => {
        const newData = {
            ...question_details,
            replies: [...question_details.replies],
        };
        const replyIndex = question_details.replies.findIndex(
            (rep) => rep.id === data.id
        );
        if (!newData.replies[replyIndex].has_liked) {
            newData.replies[replyIndex].has_liked = true;
            newData.replies[replyIndex].like_number++;
        }
        if (newData.replies[replyIndex].has_disliked) {
            newData.replies[replyIndex].has_disliked = false;
            newData.replies[replyIndex].dislike_number--;
        }
        reactToComment(question_details.id, newData, replyIndex);
    };

    const negativeReaction = () => {
        const newData = {
            ...question_details,
            replies: [...question_details.replies],
        };
        const replyIndex = question_details.replies.findIndex(
            (rep) => rep.id === data.id
        );
        if (!newData.replies[replyIndex].has_disliked) {
            newData.replies[replyIndex].has_disliked = true;
            newData.replies[replyIndex].dislike_number++;
        }
        if (newData.replies[replyIndex].has_liked) {
            newData.replies[replyIndex].has_liked = false;
            newData.replies[replyIndex].like_number--;
        }
        reactToComment(question_details.id, newData, replyIndex);
    };

    return (
        <div className={styles.Item}>
            <Header
                avatar={data.avatar}
                isReply={isReply}
                name={data.name}
                title={data.title}
                replies={data.replies}
                has_disliked={data.has_disliked}
                dislike_number={data.dislike_number}
                has_liked={data.has_liked}
                like_number={data.like_number}
                date={data.date}
                time={data.time}
            />

            <Contents
                description={data.description}
                image={data.image}
                id={data.id}
                noPicture={noPicture}
                detailButton={detailButton}
                isReply={isReply}
                negativeReaction={negativeReaction}
                positiveReaction={positiveReaction}
            />
        </div>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        reactToComment: (id, newData) => dispatch(ReplyReaction(id, newData)),
    };
};

Message.propTypes = {
    data: PropTypes.shape({
        avatar: PropTypes.string,
        date: PropTypes.string,
        description: PropTypes.string,
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        image: PropTypes.string,
        replies: PropTypes.array,
        time: PropTypes.string,
        title: PropTypes.string,
        dislike_number: PropTypes.number,
        has_disliked: PropTypes.bool,
        like_number: PropTypes.number,
        has_liked: PropTypes.bool,
    }),
    noPicture: PropTypes.bool,
    isReply: PropTypes.bool,
    detailButton: PropTypes.bool,
    question_details: PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        title: PropTypes.string,
        description: PropTypes.string,
        avatar: PropTypes.string,
        image: PropTypes.string,
        replies: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
                name: PropTypes.string,
                avatar: PropTypes.string,
                description: PropTypes.string,
                date: PropTypes.string,
                time: PropTypes.string,
                like_number: PropTypes.number,
                has_liked: PropTypes.bool,
                dislike_number: PropTypes.number,
                has_disliked: PropTypes.bool,
            })
        ),
        date: PropTypes.string,
        time: PropTypes.string,
    }),
};

export default connect(null, mapDispatchToProps)(Message);
