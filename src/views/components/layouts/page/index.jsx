import React from "react";
import styles from "./page.module.css";
import Navbar from "../../navbar";
import PropTypes from "prop-types";

const Page = (props) => {
    const { navbar = {}, children } = props;
    return (
        <div className={styles.Page}>
            {/* header */}
            <Navbar {...navbar} />

            {/* content of website */}
            <div className={styles.ContentContainer}>{children}</div>
        </div>
    );
};

Page.propTypes = {
    navbar: PropTypes.object.isRequired,
};

export default Page;
