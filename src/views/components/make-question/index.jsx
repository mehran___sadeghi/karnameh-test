import React, { useState } from "react";
import styles from "./make-question.module.css";
import Button from "../ui/button";
import Modal from "../ui/modal";
import QuestionForm from "./question-form";
import { AddNewQuestion } from "../../../store/actions/questions";
import { connect } from "react-redux";

// icons
import { ReactComponent as PlusIcon } from "./../../../assets/svg/plus.svg";

const MakeQuestion = (props) => {
    const { addQuestion } = props;

    const [modalOpen, setModalOpen] = useState(false);

    const submitForm = (subject, text) => {
        addQuestion(subject, text);
    };

    return (
        <>
            <Button
                onClick={() => setModalOpen(true)}
                className={styles.Button}
                btn_fill
                btn_style={{ width: "130px" }}
            >
                <span>سوال جدید</span>
                <PlusIcon className={styles.PlusIcon} />
            </Button>

            {/* add question Modal */}
            <Modal visible={modalOpen} onClose={() => setModalOpen(false)}>
                {/* header of Modal */}
                <div className={styles.ModalContent}>
                    <div className={styles.ModalHeader}>
                        <div
                            onClick={() => setModalOpen(false)}
                            className={styles.CloseIconContainer}
                        >
                            <PlusIcon className={styles.CloseIcon} />
                        </div>
                        <span>ایجاد سوال جدید</span>
                    </div>

                    {/* contents of modal */}
                    <div className={styles.FormContainer}>
                        <QuestionForm
                            submitForm={submitForm}
                            setModalOpen={setModalOpen}
                        />
                    </div>
                </div>
            </Modal>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        addQuestion: (subject, text) => dispatch(AddNewQuestion(subject, text)),
    };
};

export default connect(null, mapDispatchToProps)(MakeQuestion);
