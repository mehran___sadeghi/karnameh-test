import React, { useRef } from "react";
import styles from "./question-form.module.css";
import Button from "../../ui/button";
import PropTypes from "prop-types";

const QuestionForm = (props) => {
    const { setModalOpen, submitForm } = props;
    const subjectRef = useRef(null);
    const textRef = useRef(null);

    return (
        <>
            <label className={styles.Label}>موضوع</label>
            <input ref={subjectRef} className={styles.SubjectInput} />
            <label className={styles.Label}>متن سوال</label>
            <textarea ref={textRef} className={styles.TextArea} />
            <div className={styles.ButtonsContainer}>
                <Button
                    onClick={() => {
                        submitForm(
                            subjectRef.current.value,
                            textRef.current.value
                        );
                        setModalOpen(false);
                    }}
                    label={"ایجاد سوال"}
                    btn_fill
                    className={styles.SubmitButton}
                    btn_style={{ width: "100px" }}
                />
                <Button
                    label={"انصراف"}
                    btn_fill={false}
                    onClick={() => setModalOpen(false)}
                    btn_style={{
                        width: "100px",
                        color: "var(--kar-color-primary)",
                        colorHover: "var(--kar-color-primary-hover)",
                        colorActive: "var(--kar-color-primary-active)",
                        themeColor: "transparent",
                    }}
                />
            </div>
        </>
    );
};

QuestionForm.propTypes = {
    setModalOpen: PropTypes.func,
    submitForm: PropTypes.func,
};

export default React.memo(QuestionForm);
