import React from "react";
import styles from "./navbar.module.css";
import PropTypes from "prop-types";
import MakeQuestion from "../make-question";

// icons
import { ReactComponent as BottomArrowIcon } from "./../../../assets/svg/bottom-arrow.svg";

const Navbar = (props) => {
    const { title = "" } = props;
    return (
        <div className={styles.NavbarContainer}>
            <div className={styles.Navbar}>
                <div className={styles.LeftContainer}>
                    <div
                        className={`${styles.ProfileContainer} ${styles.HideInMobile}`}
                    >
                        <BottomArrowIcon />
                        <span>الناز شاکردوست</span>
                        <div className={styles.ImageContainer}>
                            <img
                                width={44}
                                height={44}
                                src={
                                    "https://as1.ftcdn.net/v2/jpg/03/67/46/48/1000_F_367464887_f0w1JrL8PddfuH3P2jSPlIGjKU2BI0rn.jpg"
                                }
                                alt="avatar"
                            />
                        </div>
                    </div>
                    <MakeQuestion />
                </div>
                <h1 className={styles.Title}>{title}</h1>
            </div>
        </div>
    );
};

Navbar.propTypes = {
    title: PropTypes.string.isRequired,
};

export default Navbar;
