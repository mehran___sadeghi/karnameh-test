import React, { useRef } from "react";
import styles from "./button.module.css";
import PropTypes from "prop-types";

const Button = (props) => {
    const {
        label = "",
        btn_fill = true,
        btn_style = {},
        children = null,
        className = "",
        onClick,
    } = props;

    // make unique className to avoid confilict
    const btnClassName = useRef(
        [...new Array(7)]
            .map((i) => parseInt(Math.random() * 50).toString(32))
            .join("")
    );

    // main colors of button
    const theme = {
        main: btn_style?.themeColor || "var(--kar-color-primary)",
        hover: btn_style?.themeColorHover || "var(--kar-color-primary-hover)",
        active:
            btn_style?.themeColorActive || "var(--kar-color-primary-active)",
    };

    return (
        <>
            {/* 
            the button has three classNames. 
            one is btnClassName. 
            on is for static styles in css file.
            and the last one is passed from props for customizing the button. (optional)
        */}
            <button
                onClick={onClick}
                className={`${styles.Button} btn-${btnClassName.current} ${className}`}
            >
                {children || label}
            </button>
            <style jsx={"true"}>{`
                .btn-${btnClassName.current} {
                    height: ${btn_style.height || "36px"};
                    width: ${btn_style.width || "auto"};
                    border-color: ${theme.main};
                    background-color: ${btn_fill ? theme.main : "transparent"};
                    color: ${btn_fill
                        ? btn_style?.color || "white"
                        : btn_style?.color || theme.main};
                }
                .btn-${btnClassName.current}:hover {
                    border-color: ${theme.hover};
                    background-color: ${btn_fill ? theme.hover : "transparent"};
                    color: ${btn_fill
                        ? btn_style?.colorHover || btn_style.color || "white"
                        : btn_style?.colorHover ||
                          btn_style.color ||
                          theme.hover};
                }
                .btn-${btnClassName.current}:active {
                    border-color: ${theme.active};
                    background-color: ${btn_fill
                        ? theme.active
                        : "transparent"};
                    color: ${btn_fill
                        ? btn_style?.colorActive || btn_style.color || "white"
                        : btn_style?.colorActive ||
                          btn_style.color ||
                          theme.active};
                }
            `}</style>
        </>
    );
};

Button.propTypes = {
    onClick: PropTypes.func,
    label: PropTypes.string,
    btn_fill: PropTypes.bool,
    className: PropTypes.string,
    btn_style: PropTypes.shape({
        height: PropTypes.string,
        width: PropTypes.string,
        themeColor: PropTypes.string,
        themeColorHover: PropTypes.string,
        themeColorActive: PropTypes.string,
        color: PropTypes.string,
        colorHover: PropTypes.string,
        colorActive: PropTypes.string,
    }),
};

export default React.memo(Button);
