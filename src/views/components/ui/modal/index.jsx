import React, { useEffect, useState } from "react";
import styles from "./modal.module.css";
import PropTypes from "prop-types";

/**
 * timeToVisible: (milliseconds) time to first render the children
 */
const Modal = (props) => {
    const {
        visible,
        onClose,
        children,
        timeToVisible = 100,
        transition = 500,
    } = props;

    const [show, setShow] = useState(false);

    useEffect(() => {
        setTimeout(
            () => {
                setShow(visible);
            },
            visible ? timeToVisible : transition
        );
    }, [visible]); // eslint-disable-line

    // if none of these are true, dont render anything in dom
    if (!visible && !show) {
        return null;
    }

    return (
        <>
            <div
                onClick={() => onClose()}
                className={`${styles.BackDrop} ModalBackDrop ${
                    visible && show ? styles.Show : styles.Hide
                }`}
            />
            <div
                className={`${styles.Modal} MainModal ${
                    visible && show ? styles.Show : styles.Hide
                }`}
            >
                {children}
            </div>
            <style jsx={"true"}>{`
                .ModalBackDrop {
                    transition: opacity ${transition}ms;
                }
                .MainModal {
                    transition: opacity ${transition}ms,
                        transform ${transition}ms, width ${transition}ms;
                }
            `}</style>
        </>
    );
};

Modal.propTypes = {
    visible: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    children: PropTypes.node,
    timeToVisible: PropTypes.number,
    transition: PropTypes.number,
};

export default Modal;
