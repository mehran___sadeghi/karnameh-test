import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router";
import { getQuestionDetails } from "../../../store/actions/questions";
import Page from "../../components/layouts/page";
import Message from "../../components/message";
import AnswerBox from "./answer-box";
import styles from "./question-details.module.css";
import PropTypes from "prop-types";
import Button from "../../components/ui/button";

const QuestionDetails = (props) => {
    const { question_details, loading, error, getData } = props;

    const params = useParams();

    useEffect(() => {
        getData(params.id);
    }, []); // eslint-disable-line

    return (
        <Page navbar={{ title: "جزییات سوال" }}>
            {!loading && !error && question_details !== null && (
                <>
                    {/* main question */}
                    <Message isReply={false} data={question_details} />

                    {/* replies */}
                    <h2 className={styles.AnswersLabel}>پاسخ ها</h2>
                    {(question_details.replies || []).length === 0 && (
                        <div className={styles.AnswerEmpty}>
                            پاسخی داده نشده است
                        </div>
                    )}
                    {(question_details.replies || []).map((reply) => {
                        return (
                            <Message
                                isReply
                                key={reply.id}
                                data={reply}
                                question_details={question_details}
                            />
                        );
                    })}

                    {/* bottom form for reply */}
                    <AnswerBox id={question_details.id} />
                </>
            )}
            {loading && <div className={styles.Loading}>در حال پردازش...</div>}
            {!loading && error && (
                <>
                    <div className={styles.Error}>{error}</div>
                    <Button
                        onClick={() => getData(params.id)}
                        label={"دوباره تلاش کنید"}
                        btn_fill
                        btn_style={{ width: "160px" }}
                    />
                </>
            )}
        </Page>
    );
};

const mapStateToProps = (state) => {
    return {
        loading: state.questions.loading,
        error: state.questions.error,
        question_details: state.questions.question_details,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getData: (id) => dispatch(getQuestionDetails(id)),
    };
};

QuestionDetails.propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.string,
    question_details: PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        title: PropTypes.string,
        description: PropTypes.string,
        avatar: PropTypes.string,
        image: PropTypes.string,
        replies: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
                name: PropTypes.string,
                avatar: PropTypes.string,
                description: PropTypes.string,
                date: PropTypes.string,
                time: PropTypes.string,
                like_number: PropTypes.number,
                has_liked: PropTypes.bool,
                dislike_number: PropTypes.number,
                has_disliked: PropTypes.bool,
            })
        ),
        date: PropTypes.string,
        time: PropTypes.string,
    }),
};

export default connect(mapStateToProps, mapDispatchToProps)(QuestionDetails);
