import React, { useRef } from "react";
import { connect } from "react-redux";
import { AddComment } from "../../../../store/actions/questions";
import Button from "../../../components/ui/button";
import styles from "./answer-box.module.css";
import PropTypes from "prop-types";

const AnswerBox = (props) => {
    const { id, addComment } = props;

    const textAreaRef = useRef(null);

    const submit = () => {
        addComment(id, textAreaRef.current.value);
        textAreaRef.current.value = "";
    };

    return (
        <>
            <h2 className={styles.Title}>پاسخ خود را ثبت کنید</h2>
            <label className={styles.Label}>پاسخ خود را بنویسید</label>
            <textarea
                ref={textAreaRef}
                className={styles.TextArea}
                placeholder={"متن پاسخ..."}
            />
            <Button
                onClick={submit}
                label={"ارسال پاسخ"}
                btn_fill
                btn_style={{ width: "200px" }}
                className={styles.Button}
            />
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        addComment: (id, text) => dispatch(AddComment(id, text)),
    };
};

AnswerBox.propTypes = {
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default connect(null, mapDispatchToProps)(AnswerBox);
