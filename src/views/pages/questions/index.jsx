import React, { useEffect } from "react";
import styles from "./questions.module.css";
import Page from "../../components/layouts/page";
import { connect } from "react-redux";
import { getQuestions } from "../../../store/actions/questions";
import Message from "../../components/message";
import PropTypes from "prop-types";
import Button from "../../components/ui/button";

const Questions = (props) => {
    const { getData, error, loading, questions } = props;

    useEffect(() => {
        getData();
    }, []); // eslint-disable-line

    return (
        <Page navbar={{ title: "لیست سوالات" }}>
            {!error &&
                !loading &&
                questions.length > 0 &&
                questions.map((question) => {
                    return (
                        <Message
                            detailButton
                            noPicture
                            data={question}
                            key={question.id}
                        />
                    );
                })}
            {!error && !loading && questions.length === 0 && (
                <div className={styles.NoQuestions}>سوالی وجود ندارد</div>
            )}
            {loading && <div className={styles.Loading}>در حال پردازش...</div>}
            {!loading && error && (
                <>
                    <div className={styles.Error}>{error}</div>
                    <Button
                        onClick={getData}
                        label={"دوباره تلاش کنید"}
                        btn_fill
                        btn_style={{ width: "160px" }}
                    />
                </>
            )}
        </Page>
    );
};

const mapStateToProps = (state) => {
    return {
        loading: state.questions.loading,
        error: state.questions.error,
        questions: state.questions.questions,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getData: () => dispatch(getQuestions()),
    };
};

Questions.propTypes = {
    error: PropTypes.string,
    loading: PropTypes.bool,
    questions: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
            title: PropTypes.string,
            description: PropTypes.string,
            avatar: PropTypes.string,
            image: PropTypes.string,
            replies: PropTypes.arrayOf(
                PropTypes.shape({
                    id: PropTypes.oneOfType([
                        PropTypes.number,
                        PropTypes.string,
                    ]),
                    name: PropTypes.string,
                    avatar: PropTypes.string,
                    description: PropTypes.string,
                    date: PropTypes.string,
                    time: PropTypes.string,
                    like_number: PropTypes.number,
                    has_liked: PropTypes.bool,
                    dislike_number: PropTypes.number,
                    has_disliked: PropTypes.bool,
                })
            ),
            date: PropTypes.string,
            time: PropTypes.string,
        })
    ),
};

export default connect(mapStateToProps, mapDispatchToProps)(Questions);
